
	var capturedData = "";
	var dataArray = [];
	var esTicket = false;
	var tipo = '';
	var fechaDesde = '';
	var fechaHasta = '';
	var dni = '';
	var nombres = '';
	var enviandoDatos = false;

	function stopDefAction(evt) {

	  if(evt.key == "Tab" || dataArray.length == 5){

		dataArray.push(capturedData);
		obtenerValores();
		
		if(!enviandoDatos)
		{
			enviandoDatos = true;
			$.ajax({
				url: './personal/buscar.php',
				type: 'POST',			
				data: { 
					tipo : tipo,
					fechaDes : fechaDesde,
					fechaHas : fechaHasta,
					documento : dni,
					nombres : nombres
				},			
				beforeSend:function(){
					$(".cargador").show();
				},
				success:function(response){
					$("#modal-content").modal("show");
					$("#titulo-modal").empty().append("<h2 style='font-size: 50px;'>"+response["titulo"].toUpperCase()+"</h2>");
					$("#contenido-modal").empty().append("<h1 style='font-size: 50px;'>"+response["mensaje"].toUpperCase()+"</h1>");
					$(".cargador").hide();			
					setTimeout(function() {						
						$("#modal-content").modal('hide');
						$('#documento').val('');
						esTicket = false;
						enviandoDatos = false;
					}, 4000);
					capturedData = "";
					
				},
				error(){
					$("#modal-content").modal("show");
					$("#titulo-modal").empty().append("<h2 style='font-size: 50px;'>No fué posible leer el documento</h2>");
					$("#contenido-modal").empty().append("<h1 style='font-size: 50px;'>Intente nuevamente</h1>");
					$(".cargador").hide();
				}
			});
		}
	  }else if(evt.key == "\""){
	  	dataArray.push(capturedData);
	  	capturedData = "";
	  }else if(evt.key == ","){
		esTicket = true;
		dataArray.push(capturedData);
		capturedData = "";
	}else if(evt.key == "Shift" || evt.key == "Alt" || evt.key == "{"){
	}else{
		capturedData += evt.key.toString();
	  }
	}

	function obtenerValores(){
		if(esTicket)
		{			
			tipo = 'visitas_ticket';
			nombres = dataArray[0];
			dni = dataArray[1];
			fechaDesde = dataArray[2];
			fechaHasta = dataArray[3];
		}else 
		{
			tipo = 'visitas_dni';
			if(dataArray.length > 8)
			{
				dni =  dataArray[1];
				nombres = dataArray[4]+'-'+dataArray[5]; 
			}else{
				nombres = dataArray[1]+'-'+dataArray[2]; 
				dni =  dataArray[4];
			}
		}

		dataArray = [];
	}

